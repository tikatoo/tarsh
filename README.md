# tarsh - self-extracting archives in shell script format #

Tarsh is a kind of silly thing that isn't really needed.
It's intended to make my life ever so slightly easier.

Tarsh outputs a shell script with a tarball stuck on the end.
Executing this script extracts the tarball and,
if configured to do so, runs a program.

It is recommended to suffix these scripts with `.tar.sh`.
The suffix doesn't need to include the compression method,
since that information is explicitly encoded in the extract script.

Passing `-x` to the self-extractor will simply extract the archive,
and suppress any automatic program execution.


### PLEASE DO NOT DISTRIBUTE SELF-EXTRACTING ARCHIVES ###

This is a convenience tool for internal use.
Users are strongly discouraged from running code they don't trust,
and a self-extracting archive can be difficult to examine.
Tarsh in particular generates files
that can't be trivially extracted without executing them,
which makes them unsuitable for use in distribution.

## Installing ##

Download the `tarsh` file and put it somewhere on your PATH
(I recommend `~/.local/bin`).

You can also include `tarsh` in the archive you're creating,
and invoke it from the configured program.
In this way, you could have a semi-standalone app
that extracts itself, and when it's done,
compresses itself again.
